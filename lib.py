# -*- coding: utf-8 -*-
from __future__ import print_function
import requests


def make_tintyurl(long_url):
    tinyurl = "https://tinyurl.com/api-create.php?url=%s" % long_url.strip()
    rsp = requests.get(tinyurl)
    return rsp.text

def tracking(f):
    def d_f(*args, **kargs):
        #if f.__name__ == "get_sh":
        #    print(f.__name__, kargs['uri']).encode('utf-8')
        #else:
        #    print(f.__name__).encode('utf-8')
        result = f(*args, **kargs)
        return result
    return d_f
