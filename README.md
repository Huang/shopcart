
## Install

   - System Package
     - sudo apt-get install libncurses5-dev
     - sudo apt-get install libxml2-dev libxslt1-dev
     - sudo apt-get install python-dev libffi-dev libssl-dev 

   - Python Package 
     - virtualenv .
     - source bin/activate 
     - pip install -r requirements.txt 
    
   - Redis(CLUSTER MODE)
     - http://redis.io/topics/quickstart
     - NOTE: Redis is only accessible within local lan

   - Known Issues
     - use 'requests[security]' or just use requests==2.5.3 instead
        - Reference:
           - http://stackoverflow.com/questions/29099404/ssl-insecureplatform-error-when-using-requests-package
     - problem while installing scrapy on osx
        - Reference:
           - https://www.google.com.tw/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=pip+install+functools32+requirements.txt
           - https://cryptography.io/en/latest/installation/#using-your-own-openssl-on-os-x
           - https://github.com/eventmachine/eventmachine/issues/602

## Configuration

   - Config settings
     - cp settings.sample.py settings.py 
     - modify settings.py according to your env

   - Config access rights to google sheet
     - Assign credential path to settings.py
     - Assign sheet name in settings.py
 
## Execution
   - Process order 
     - python control.py

   - Process payment
     - python paid_sheet_grouping.py

   - Process shipping
     - python ship_form.py

