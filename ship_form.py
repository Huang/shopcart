# -*- coding: utf-8 -*-
import lang
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from pprintpp import pprint as pp
from dictsheet import DictSheet
import copy
import time
import settings
import re
import urllib
from collections import defaultdict
import traceback
import datetime
from dateutil.relativedelta import relativedelta


class ShipManager(object):

    def __init__(self, **kwargs):
        self.col_map = {
            u"序號": self.get_sn,
            u"出貨單號": self.get_shipping_sn,
            u"件數": self.get_shipping_item_counts,
            u"收件人": self.get_addressee,
            u"電話": self.get_tel,
            u"手機": self.get_cellphone,
            u"郵遞區號": self.get_zipcode,
            u"地址": self.get_address,
            u"希望送達日": self.get_arriving_date,
            u"希望送達時段": self.get_arriving_time,
            u"代收款": self.get_collection_fee,
            u"內容物": self.get_container,
            u"溫層": self.get_self_preservation,
            u"備註": self.get_note
        }
        if 'sh' not in kwargs or 'dishes_info' not in kwargs:
            raise

        self.sh = kwargs['sh']
        self.freeze_wks = self.sh.worksheet(u"冷凍")
        self.clear_sheet(wks=self.freeze_wks)
        self.freeze_dwks = DictSheet(self.freeze_wks)
        self.freeze_dwks.__len__ = 1

        self.cold_wks = self.sh.worksheet(u"冷藏")
        self.clear_sheet(wks=self.cold_wks)
        self.cold_dwks = DictSheet(self.cold_wks)
        self.cold_dwks.__len__ = 1

        self.merge_wks = self.sh.worksheet(u"合併單")
        self.clear_sheet(wks=self.merge_wks)
        self.merge_dwks = DictSheet(self.merge_wks)
        self.merge_dwks.__len__ = 1

        self.dishes_info = kwargs['dishes_info']
        print("finish loading")

    def clear_sheet(self, wks):
        cell_list = wks.range('A2:M100')
        for cell in cell_list:
                cell.value = ''
        wks.update_cells(cell_list)

    def if_exist(self, order):
        return False

    def get_preservation(self, order):
        dishs = self.dishes_info['Preservation'].keys()
        preservation_list = []
        for dish in dishs:
            if dish not in order or order[dish] == "":
                continue
            preservation_list.append(self.dishes_info['Preservation'][dish])
        if u"必凍" in preservation_list:
            return u"冷凍"
        if u"冷藏" in preservation_list:
            return u"冷藏"
        return u"冷凍"

    def find_paid_row(self, order, paid_dwks):
        for idx, row in paid_dwks.iteritems():
            if row[lang.order_sn] != order[lang.order_sn]:
                continue
            if row[lang.order_sn] == order[lang.order_sn]:
                return row
        # 歷史因素可能不存在
        return None 


    def add_shipping_record(self, order, paid_dwks):
        print(order[lang.order_sn])
        try:
            update_dwks = self.cold_dwks
            preservation = self.get_preservation(order)
            if u"冷凍" == preservation:
                update_dwks  = self.freeze_dwks
            self.preservation = preservation
            new_shipping_record = {}
            paid_row = self.find_paid_row(order, paid_dwks)
            if paid_row == None:
                print("paid_row is none", order[lang.order_sn])
                return False
            for k, v in self.col_map.items():
                val = v(order=order, paid_row=paid_row)
                new_shipping_record[k] = val
            #new_shipping_record[u'物流'] = order[lang.order_delivery]
            update_dwks.append(new_shipping_record)
            self.merge_dwks.append(new_shipping_record)
        except:
            pp(traceback.format_exc())

    def get_sn(self, order, paid_row):
        if self.preservation == u"冷凍":
            return self.freeze_dwks.__len__
        else:
            return self.cold_dwks.__len__

    def get_shipping_sn(self, order, paid_row):
        return order[lang.order_sn]

    def get_shipping_item_counts(self, order, paid_row):
        if "+" not in order['Size']:
            return 1
        return len(order['Size'].split("+"))
        """
        dishes = self.dishes_info['Preservation'].keys()
        counter = 0
        for dish in dishes:
            if dish not in order or order[dish] == "":
                continue
            if dish in order:
                counter += int(order[dish])
        return counter
        # convert to int (skip 後五碼+運費)
        """
    def get_addressee(self, order, paid_row):
        return paid_row[u"收件人"]
        
    def get_tel(self, order, paid_row):
        return paid_row[u"市話"]

    def get_cellphone(self, order, paid_row):
        return paid_row[u"手機"]

    def get_zipcode(self, order, paid_row):
        if u"郵遞區號" not in paid_row:
            return ""
        return paid_row[u"郵遞區號"]

    def get_address(self, order, paid_row):
        if order[lang.order_delivery] == u"黑貓":
            tmp = paid_row[u"收件地址"]
        else:
            tmp = u"" +  paid_row[u"收件地址"]
        return tmp

        
    def get_arriving_date(self, order, paid_row):
        today = datetime.datetime.today()
        if paid_row[u"希望收件日"] in [u"平日", u"不指定"]:
            #arriving_date = today + relativedelta(days=1)
            weekday = today.weekday()
            esp_days = 6 - weekday - 2
            arriving_date = today + relativedelta(days=esp_days)
        else:
            weekday = today.weekday()
            # Monday is 0 and Sat is 5 , Sunday is 6
            if weekday < 5:
                # 假設如果是平日祭出，則設定週六到
                esp_days = 6 - weekday - 1
                arriving_date = today + relativedelta(days=esp_days)
            elif weekday == 5:
                # 這邊是假設如果是 六 才要寄出，就隔日到(週日)
                arriving_date = today + relativedelta(days=1)
            else:
                # 這邊是假設如果是 日 才要寄出，就週六到
                arriving_date = today + relativedelta(days=6)
        return arriving_date.strftime('%Y%m%d')

    def get_arriving_time(self, order, paid_row):
    
        if paid_row[u"希望收件時段"] in [u"不指定"]:
            return 3
        if paid_row[u"希望收件時段"] in [u"早 13:00前"]:
            return 1
        elif paid_row[u"希望收件時段"] in [u"午 14:00-18:00"]:
            return 2
        #elif paid_row[u"希望收件時段"] == u"17 - 20 時":
        #    return 3
        # 不指定
        return 0

    def get_collection_fee(self, order, paid_row):
        if order[lang.order_cod] == u"是":
            return order[lang.order_amount].split("(")[0].strip()
        else:
            return 0
    def get_self_preservation(self, order, paid_row):

        #1.常溫2.冷藏3.冷凍 
        if self.preservation == u'冷藏':
            return 2
        if self.preservation == u'冷凍':
            return 3
        return 1
    
    def get_order_abbr(self, order, paid_row):
        abbr_list = []
        dishes = self.dishes_info['Abbr'].keys()
        for dish in dishes:
            if dish not in order or order[dish] == "":
                continue
            if dish in order:
                sums = sum([int(s) for s in re.findall("\d+", order[dish])])
                if order[dish].isdigit():
                    abbr_str = "%s%s" % (sums, self.dishes_info['Abbr'][dish])
                else:
                    abbr_str = "%s%s(%s)" % (sums, self.dishes_info['Abbr'][dish], order[dish])
                #abbr_str = "%s%s" % (int(order[dish]), self.dishes_info['Abbr'][dish])
                print(abbr_str.encode('utf-8'))
                abbr_list.append(abbr_str)
        return ", ".join(abbr_list)


    def get_container(self, order, paid_row):
        return u"湯品"
    def get_note(self, order, paid_row):
        abbr_str = self.get_order_abbr(order, paid_row)
        #return u"%s %s" % (self.preservation[1], abbr_str)
        return u"%s" %  abbr_str


def get_dish_info(dishes_dwks):
    # input dishes_dwks is a wks
    #dishes_size , dishes_price = {}, {}
    dishes_info = {}
    #dishes_info['size'], dishes_info['price'], 
    dishes_info['Preservation'] = {}
    dishes_info['Abbr'] = {}
    for idx, row in dishes_dwks.iteritems():
        if idx == 1:
            continue
        #dishes_info['size'][row['Dishes']] = float(row['Size'])
        #dishes_info['price'][row['Dishes']] = int(row['Price'])
        dishes_info['Preservation'][row['Dishes']] = row['Preservation']
        dishes_info['Abbr'][row['Dishes']] = row['Abbr']
    return dishes_info

def process_order(row, orders, dishes_info, paid_dwks, ship_manager):
	for idx, order in orders.iteritems():
		print(order)
		if order[lang.order_status].strip() in ['ok'] and order[lang.order_delivery].strip() in [u'宅配通', u'黑貓', u'自取']:
			if not ship_manager.if_exist(order):
				#print ship_manager.if_exist(order)
				ship_manager.add_shipping_record(order=order, paid_dwks=paid_dwks)
				continue
		else:
			print('missing record', order[lang.order_status], order[lang.order_delivery])


def process_control():
    # scan control sheet
    control_sh = get_sh(uri=settings.CONTROL_SHEET)
    parts = DictSheet(control_sh.worksheet("parts"))
    #config_dwks = DictSheet(control_sh.worksheet("config"))
    dish_dwks = DictSheet(wks=control_sh.worksheet(settings.CONTROL_SHEET_DISHES))
    dishes_info = get_dish_info(dish_dwks)
    order_sh = get_sh(uri=settings.ORDER_SHEET)
    paid_sh = get_sh(uri=settings.PAID_SHEET)
    shipping_sh = get_sh(uri=settings.SHIPPING_SHEET)
    ship_manager = ShipManager(sh=shipping_sh, dishes_info=dishes_info)

    paid_sheet_list = paid_sh.worksheets()
    pno_sheet_map = {}

    for sheet in paid_sheet_list:
        pno_sheet_map[sheet.title] = sheet

    for idx, part in parts.items():
        #pp(part)
        part[lang.last_check_time] = time.ctime()
        if part[lang.close] == "Y":
            continue
        if part['No'] not in pno_sheet_map:
            print(part['No'],"not in pno_sheet_map")
            continue
        try:
            # TO-DO: Retry need to convert to int
            if part[lang.order_status] != "OK" and part['Retry'] < 10:
                pass
            if part[lang.order_status] == "OK":
                pp(part['No'])
                orders = DictSheet(wks=order_sh.worksheet(part['No']))
                #dishes_info['limit'] = get_limit(limit_dwks, part['No'])
                paid_dwks = DictSheet(wks=pno_sheet_map[part['No']])
                #hidden_wks = get_hidden_wks(hidden_sh, order)
                process_order(row=part, orders=orders, dishes_info=dishes_info, paid_dwks=paid_dwks, ship_manager=ship_manager)
                #generate_hidden_sheet(part, orders, hidden_sh)
                #generate_fb_push(orders=orders, fb_token)
            part[u'宅配單檢查'] = "YES"
        except:
            msg = u"NO, Call Chandler ASAP. \n" + traceback.format_exc()
            part[u'宅配單檢查'] = msg

def get_sh(uri, url=None):
    scope = ['https://spreadsheets.google.com/feeds']
    credentials = ServiceAccountCredentials.from_json_keyfile_name('facebook-shop-cart-acdb9ee1b37e.json', scope)
    gc = gspread.authorize(credentials)
    if url is None:
        sh = gc.open(uri)
    else:
        sh = gc.open_by_key(uri)
    return sh

process_control()
