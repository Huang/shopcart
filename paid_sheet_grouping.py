# -*- coding: utf-8 -*-
import lang
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from pprintpp import pprint as pp
from dictsheet import DictSheet
import copy
import time
import settings
import re
import urllib
from collections import defaultdict
from lib import tracking

def process_control():
    # scan control sheet
    paid_sh = get_sh(uri=settings.PAID_SHEET)
    worksheet_list = paid_sh.worksheets()
    pp(worksheet_list)
    pno_sheet_map = {}
    for sheet in worksheet_list:
        pno_sheet_map[sheet.title] = sheet
    paid_dwks = DictSheet(wks=paid_sh.get_worksheet(0))
    #import json
    #with open('data.txt', 'w') as outfile:
    #    json.dump(paid_dwks, outfile)
    sub_payment_sheet_map = {}
    for idx, payment in paid_dwks.iteritems():
        pp(payment)
        pno = payment[lang.order_sn].split(u'-')[0]
        if pno == "" or pno not in pno_sheet_map:
            pp("MISS MATCH")
            continue
        add_flag = False
        if pno not in sub_payment_sheet_map:
            sub_payment_sheet = DictSheet(wks=pno_sheet_map[pno])
            sub_payment_sheet_map[pno] = sub_payment_sheet
        for _idx, _payment in sub_payment_sheet_map[pno].iteritems():
            if _payment[lang.order_sn] == payment[lang.order_sn]:
                add_flag = True
        if not add_flag:
            pp("Adding Row......")
            pp(payment)
            sub_payment_sheet_map[pno].append(payment)




@tracking
def get_sh(uri, url=None):
    scope = ['https://spreadsheets.google.com/feeds']
    credentials = ServiceAccountCredentials.from_json_keyfile_name('facebook-shop-cart-acdb9ee1b37e.json', scope)
    gc = gspread.authorize(credentials)
    if url is None:
        sh = gc.open(uri)
    else:
        sh = gc.open_by_key(uri)
    return sh

process_control()
