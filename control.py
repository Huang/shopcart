# -*- coding: utf-8 -*-

from __future__ import print_function
import lang
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from pprintpp import pprint as pp
from dictsheet import DictSheet
import copy
import time
import settings
import re
import lib
from lib import tracking
import urllib
from collections import defaultdict
import traceback
import sys
reload(sys)
sys.setdefaultencoding('utf-8')


@tracking 
def get_total_order_counts(order_dish):
    """
    order_dish is like  3補1 , 2贈1 or 2補1贈1, 補1, 贈2 
    return {'total': 5, 'order': 3, 'give':1 , 'appendix': 1}
    """
    order_pat = re.compile(ur'^(\d+)', re.UNICODE)
    appendix_pat = re.compile(ur'補(\d+)', re.UNICODE)
    give_pat = re.compile(ur'贈(\d+)', re.UNICODE)
    pat_mapping = {'order': order_pat, 'appendix': appendix_pat, 
                   'give': give_pat}
    result = {}
    total = 0
    for order_type, pat in pat_mapping.items():
        match = re.search(pat, order_dish)
        if match is None:
            result[order_type] = 0
        else:
            result[order_type] = int(match.group().replace(u'補','').replace(u'贈',''))
        total += result[order_type]
    result['total'] = total
    return result


@tracking
def retry_inc(part):
    # part is the row in parts wks of control sheet
    retry = part['Retry']
    if retry is "":
        retry = 0
    else:
        retry = int(retry)
    part['Retry'] = retry + 1

@tracking
def init_hidden_sheet(pno, hidden_sh, limit):
    try:
        worksheet_list = hidden_sh.worksheets()
        hit = False
        for wks in worksheet_list:
            if wks.title == pno:
                hit = True
        pp(pno)
        if not hit:
            hidden_wks = hidden_sh.add_worksheet(title=pno, rows="100", cols="26")
        else:
            hidden_wks = hidden_sh.worksheet(pno)
        hidden_dwks = DictSheet(wks=hidden_wks)
        new_cols = [u'', u'Timestamp', lang.fb_id, lang.order_memo]
        new_cols.extend(get_dishs(pno, limit))
        hidden_dwks.mapping = new_cols
    except Exception as e:
        print("init_hidden_sheet error: ", pno)
        pp(str(e))
        return False
    return True

@tracking 
def get_dish_info(dishes_dwks):
    # input dishes_dwks is a wks
    #dishes_size , dishes_price = {}, {}
    dishes_info = {}
    dishes_info['size'], dishes_info['price'], dishes_info['Preservation'] = {}, {}, {}
    for idx, row in dishes_dwks.iteritems():
        if idx == 1:
            continue
        print(idx, row['Dishes'], row['Price'])
        dishes_info['size'][row['Dishes']] = float(row['Size'])
        dishes_info['price'][row['Dishes']] = int(row['Price'])
        dishes_info['Preservation'][row['Dishes']] = row['Preservation']
    return dishes_info


@tracking 
def get_limit(limit_dwks, pno):
    dishes_info = {}
    dishes_info['limit'] = {}
    for idx, row in limit_dwks.iteritems():
        if pno not in row:
            raise ValueError(u'>> [ERROR]  limit for [%s] not exists. \
                             \n>> [To-Fix] Please add limit for [%s] to fix it.'% (pno, pno))
        if idx == 1:
            continue
        try:
            dishes_info['limit'][row['Dishes']] = int(row[pno])
        except ValueError as e:
            dishes_info['limit'][row['Dishes']] = 0
    #return dishes_size, dishes_price
    return dishes_info['limit']

@tracking 
def process_control():
    # scan control sheet
    control_sh = get_sh(uri=settings.CONTROL_SHEET)
    print("control_sh ok")
    parts = DictSheet(control_sh.worksheet("parts"))
    print("parts ok")
    config_dwks = DictSheet(control_sh.worksheet("config"))
    print("config_dwks ok")
    limit_dwks = DictSheet(wks=control_sh.worksheet("limit"))
    print("limit_dwks ok")
    dish_dwks = DictSheet(wks=control_sh.worksheet(settings.CONTROL_SHEET_DISHES))
    print("dish_dwks  ok")
    dishes_info = get_dish_info(dish_dwks)
    print("dishs_info ok")
    order_sh = get_sh(uri=settings.ORDER_SHEET)
    print("order_sh ok")
    #user_dwks = DictSheet(wks=control_sh.worksheet(settings.CONTROL_SHEET_USERS))
    paid_sh = get_sh(uri=settings.PAID_SHEET)
    print("paid_sh ok")
    #shipping_sh = get_sh(uri=settings.SHIPPING_SHEET)
    #ship_manager = ShipManager(sh=shipping_sh, dishes_info=dishes_info)

    paid_sheet_list = paid_sh.worksheets()
    print("paid_sh_2 ok")
    pno_sheet_map = {}
    
    for sheet in paid_sheet_list:
        print("processing %s"%sheet.title)
        pno_sheet_map[sheet.title] = sheet

    for idx, part in parts.items():
        #pp(part)
        if part[lang.close] == "Y":
            continue
        part[lang.last_check_time] = time.ctime()
        if part['No'] not in pno_sheet_map:
            print(part['No'],"not in pno_sheet_map")
            continue
        try:
            # TO-DO: Retry need to convert to int 
            if part['Status'] != "OK" and part['Retry'] < 10:
            # Initial
                """
                status = init_new_groupbuy(part['No'], order_sh)
                part['Status'] = "ok" if status else "Create Sheet Fails"
                if part['Status'] != "ok":
                    retry_inc(part)
                    continue
                status = init_hidden_sheet(part['No'], hidden_sh, limit)
                part['Status'] = "ok" if status else "Create Hidden Sheet Fails"
                if part['Status'] != "ok":
                    retry_inc(part)
                    continue
                """
                pass
            if part['Status'] == "OK":
                pp(part['No'])
                orders = DictSheet(wks=order_sh.worksheet(part['No']))
                dishes_info['limit'] = get_limit(limit_dwks, part['No'])
                paid_dwks = DictSheet(wks=pno_sheet_map[part['No']])
                #hidden_wks = get_hidden_wks(hidden_sh, order)
                process_order(row=part, orders=orders, dishes_info=dishes_info, paid_dwks=paid_dwks)
                #generate_hidden_sheet(part, orders, hidden_sh)
                #generate_fb_push(orders=orders, fb_token)
            part[lang.check_result] = "YES"
        except Exception as e:
            msg = u"NO, Call Chandler ASAP.\n" + traceback.format_exc().encode('utf-8')
            part[lang.check_result] = msg + "\n" + str(e)
        #    return -1
    return 0


@tracking
def get_payment_link(order):
    payment_link = ""
    total_price = order[lang.order_amount][order[lang.order_amount].index(u")")+1:].strip()
    print('total_price ', total_price )
    with open('tinyurl.mock.txt', 'r') as mock:
        raw_data = mock.read().decode('utf-8')
        raw_data = raw_data.replace("__ORDER_ID__", order[lang.order_sn])
        raw_data = raw_data.replace("__ORDER_TOTAL__", total_price)
        if order[lang.order_cod] == u'是':
            str_encoded = urllib.quote(u"貨到付款".encode('utf-8'))
            raw_data = raw_data.replace("__LAST_FIVE_PIN__", str_encoded)
        else:
            raw_data = raw_data.replace("__LAST_FIVE_PIN__", u"")
    payment_link = lib.make_tintyurl(raw_data)
    return payment_link

@tracking 
def gen_meg(pno, order, dish_list):

    #total_price = int(order[lang.order_amount][order[lang.order_amount].index(u")")+1:].strip())
    #dish_counts = int(order[lang.order_amount][1:order[lang.order_amount].index(u",")])
    total_price = int(order[lang.order_amount][:order[lang.order_amount].index(u"(")].strip())
    dish_counts = int(order[lang.order_amount][order[lang.order_amount].index(u"(")+1:order[lang.order_amount].index(u",")])
    if order[lang.order_shipping_fee] != u'免運':
        shipping_fee = int(order[lang.order_shipping_fee])
    else:
        shipping_fee = 0
    cash_on_delivery_fee = total_price - dish_counts - shipping_fee

    with open('reply.mock.txt', 'r') as mock:
        raw_data = mock.read().decode('utf-8')
        raw_data = raw_data.replace("__PNO__", order[lang.order_sn].replace("P", ""))
        raw_data = raw_data.replace("__DISH_LIST__", "\n".join(dish_list))
        raw_data = raw_data.replace("__DISH_COUNTS__", (u"商品總額：%s" % str(dish_counts)))
        #raw_data = raw_data.replace("__SHIPPING_FEE__", shipping_fee)
        raw_data = raw_data.replace("__ALL_COUNTS__", str(total_price))
		
        if order[lang.order_cod] == u'是':
            raw_data = raw_data.replace("__SHIPPING_FEE__",
                u"%s運費+到付手續費為：%s + %s" % (order[lang.order_delivery], shipping_fee, cash_on_delivery_fee))
        else:
            raw_data = raw_data.replace("__SHIPPING_FEE__", (u"%s運費：%s" % (order[lang.order_delivery], shipping_fee)))
        raw_data += u"\n請您務必確認金額與數量，出貨將以此訂單為主，到付及匯款後的朋友都請至 __PAYMENY_LINK__ \
                     \n填寫收件人資訊，匯款帳號：彰化銀行(代號009) 帳號：9290 51 019687 00 \
                     \n出貨前一週會再通知，謝謝您！"
        raw_data = raw_data.replace("__PAYMENY_LINK__", get_payment_link(order))
    return raw_data

@tracking 
def gen_order_sn(pno, idx):
    return u"%s-%03d" % (pno, idx-1)

@tracking 
def process_single_order(pno, idx, acc, acc_eo, order, dishs, limit, price, size, paid_dwks):
    order_status = order['Status'].strip()

    print(idx)
    if idx == 1:
        return ''
    pp(u"處理訂單 Part: %s - idx: %s" % (pno, idx))

    if order_status in [lang.status_amount, lang.status_total_counts]:
        total = 0
        for dish in dishs:
            if dish not in order:
                continue
            # 總訂單
            if order_status == lang.status_amount:
                _ = acc[dish] 
            # 總金額
            else:
                _ = int(price[dish]) * int( acc[dish] - acc_eo[dish])  

            order[dish] = _
            total += _

        order[lang.order_amount] = total
        return ''

    status = ""
    total_size = 0
    is_pendding = False
    dish_list = []

    dish_stats = get_dish_stats(order, dishs)
    is_pendding = not if_stock_available(order, dishs, acc, limit, dish_stats)
    if is_pendding is False:
        #order_dish_mapping = get_total_order_counts(order[dish])
        #total_counts = order_dish_mapping['total']

        acc_eo , acc = get_acc_statistics(acc, acc_eo,  order, dishs, dish_stats)
        total_size = get_size_statistics(order, dishs, size, dish_stats)
        dish_list = get_dish_list(order, dishs, dish_stats, price)



    if order_status in [u"候補", u'已出貨', u"ok", u'手動確認匯款', u"已匯款", "已匯",
                        u"沒有訂購任何菜色", u"-"]: 
        return ''

    if order_status == "" and is_pendding is True:
        order['Status'] = u"候補"
        return ''

    if order_status == "" and is_pendding is False:
        empty_order, total_price = get_order_total(order, price, dishs, dish_stats)
        if empty_order is True: 
            #order['Status'] = u"沒有訂購任何菜色"
            # To-Do 這邊只計算訂購的，沒有看贈/補
            return ''
        contain_appredix = if_order_contain_appredix(order, price, dishs, dish_stats)
        package_size = get_package_size(total_size)
        order[lang.order_size] = "%s (%s)" % (package_size, total_size)
        shipping_fee = get_shipping_fee(order, package_size, total_price, contain_appredix)
        order[lang.order_shipping_fee] = shipping_fee
        if order[settings.PAY_BILLS_AS_THEY_ARRIVE] == u"是":
            cash_on_delivery_fee = get_cash_on_delivery_fee(order, total_price + shipping_fee, contain_appredix)
            order[lang.order_amount] = "(%s, %s, %s) %s" % ( total_price, shipping_fee,
                                                  cash_on_delivery_fee, 
                                                  total_price + shipping_fee + cash_on_delivery_fee)
        else:
            # Here
            order[lang.order_amount] = "(%s, %s) %s" % ( total_price, shipping_fee,
                                              total_price + shipping_fee)

        order_id = gen_order_sn(pno, idx)
        order[lang.order_sn] = order_id
        msg = gen_meg(pno, order, dish_list)
        order[lang.order_msg] = msg
        if order[settings.PAY_BILLS_AS_THEY_ARRIVE] == u"是":
            order['Status'] = u'未填'
        else:
            order['Status'] = u'未付'
        return ''

    if order_status == u'未付':
        paid_status, last_5_pin  = get_paid_status(order, paid_dwks)
        if paid_status == 1:
            order[u'Status'] = u"已匯"
        elif paid_status == 2:
            order[u'Status'] = u"手動確認匯款"
        order[u'匯款帳號'] = last_5_pin
        return ''
    else:
        print("ERROR NO SUCH CASE", idx, order)


@tracking
def process_order(row, orders, dishes_info, paid_dwks):
#def process_order(row, orders, dishes_info, user_dwks, paid_dwks):
    # scan order sheet
    acc = defaultdict(int)
    acc_eo = defaultdict(int)

    pno = row['No'] # Part Idx
    dishs = dishes_info['limit'].keys()
    limit = dishes_info['limit']
    price = dishes_info['price']
    size = dishes_info['size']



    for idx, order in orders.iteritems():
        if lang.order_shipping_fee not in order:
            raise Exception(u"%s 運費欄位不存在, 請將運費加入 %s"%(pno, pno))
        # TO-DO add check
        order_status = order['Status'].strip()
        print(idx)
        if idx == 1:
            continue
        pp(u"處理訂單 Part: %s - idx: %s %s" % (pno, idx, order_status))
        
        # ----- 懷疑這段會不會跑
        if order_status in [lang.status_amount, lang.status_total_counts]:
            total = 0
            for dish in dishs:
                if dish not in order:
                    continue
                # 總訂單
                if order_status == lang.status_amount:
                    _ = acc[dish] 
                # 總金額
                else:
                    _ = int(price[dish]) * int( acc[dish] - acc_eo[dish])  

                order[dish] = _
                total += _

            order[lang.order_amount] = total
            continue

        status = ""
        total_size = 0
        is_pendding = False
        dish_list = []

        dish_stats = get_dish_stats(order, dishs)
        is_pendding = not if_stock_available(order, dishs, acc, limit, dish_stats)
        if is_pendding is False:
            #order_dish_mapping = get_total_order_counts(order[dish])
            #total_counts = order_dish_mapping['total']

            acc_eo , acc = get_acc_statistics(acc, acc_eo,  order, dishs, dish_stats)
            total_size = get_size_statistics(order, dishs, size, dish_stats)
            dish_list = get_dish_list(order, dishs, dish_stats, price)
        

        '''----- 緊急patch Size 2021.07.11----
        total_size = get_size_statistics(order, dishs, size, dish_stats)
        package_size = get_package_size(total_size)
        order[lang.order_size] = "%s (%s)" % (package_size, total_size)
        '''
        if order_status in [u"候補", u'已出貨', u"ok", u'手動確認匯款', u"已匯", u"已填",
                               u"沒有訂購任何菜色", u"-"]: 
            continue

        if order_status == "" and is_pendding is True:
            order['Status'] = u"候補"
            continue

        if order_status == "" and is_pendding is False:
            empty_order, total_price = get_order_total(order, price, dishs, dish_stats)
            if empty_order is True: 
                #order['Status'] = u"沒有訂購任何菜色"
                # To-Do 這邊只計算訂購的，沒有看贈/補
                continue
            contain_appredix = if_order_contain_appredix(order, price, dishs, dish_stats)
            package_size = get_package_size(total_size)
            order[lang.order_size] = "%s (%s)" % (package_size, total_size)
            shipping_fee = get_shipping_fee(order, package_size, total_price, contain_appredix)
            order[lang.order_shipping_fee] = shipping_fee
            if order[settings.PAY_BILLS_AS_THEY_ARRIVE] == u"是":
                cash_on_delivery_fee = get_cash_on_delivery_fee(order, total_price + shipping_fee, contain_appredix)
                print("cash_on_delivery_fee:%s" % cash_on_delivery_fee)
                order[lang.order_amount] = "%s (%s, %s, %s)" % ( total_price + shipping_fee + cash_on_delivery_fee,
                                                                total_price, shipping_fee,
                                                                cash_on_delivery_fee)
            else:
                # Here
                order[lang.order_amount] = "%s (%s, %s)" % (total_price + shipping_fee, total_price, shipping_fee)

            order_id = gen_order_sn(pno, idx)
            order[lang.order_sn] = order_id
            msg = gen_meg(pno, order, dish_list)
            order[lang.order_msg] = msg
            if order[settings.PAY_BILLS_AS_THEY_ARRIVE] == u"是":
                order['Status'] = u'未填'
            else:
                order['Status'] = u'未付'

            continue

        if order_status in  [u'未付', u'未填']:
            paid_status, last_5_pin  = get_paid_status(order, paid_dwks)
            if paid_status == 1:
                if order[settings.PAY_BILLS_AS_THEY_ARRIVE] == u"是":
                    order[u'Status'] = u"已填"
                else:
                    order[u'Status'] = u"已匯"
            elif paid_status == 2:
                order[u'Status'] = u"手動確認匯款"
            order[u'匯款帳號'] = last_5_pin
            continue 
            
        try:
            # TO-DO: 完成重構
            process_single_order(pno, idx, acc, acc_eo, order, dishs, limit, price, size, paid_dwks)
        except Exception as e:
            print(str(e), traceback.format_exc())
            raise Exception(pno, idx, e.args, traceback.format_exc())


@tracking
def get_dish_stats(order, dishs):
    dish_stats = {}
    for dish in dishs:
        if dish not in order or order[dish].strip() == "":
            continue
        dish_stats[dish] = get_total_order_counts(order[dish])
    return dish_stats

@tracking 
def get_dish_list(order, dishs, dish_stats, price):
    """
    [
        1 份水晶肉
        1 份仙草雞

    ]
    [
        破布子魚露 ＄50 ＊1 
        麻辣鴨血臭豆腐大 $420 ＊2 
    ]

    """
    dish_list = []
    str_mapping = {'appendix': u'補',
                   'give': u'贈'}
    for dish, dish_stat in dish_stats.items():
        if dish_stat['total'] == 0:
            continue
        total_counts = dish_stat['total']
        append_str = ""
        for k, v in dish_stat.items():
            if k in ['total', 'order'] or v == 0:
                continue
            # 贈2 補1
            append_str += "%s%s" % (str_mapping[k],v)
        if append_str == "":
            #dish_list.append(u"%s 份%s" % (total_counts, dish))
            dish_list.append(u"%s $%s ＊%s" % (dish, price[dish], total_counts))
        else:
            #dish_list.append(u"%s 份(%s) %s" % (total_counts, append_str,dish))
            dish_list.append(u"%s %s ＊%s" % (dish, append_str, total_counts))
    return dish_list 

@tracking 
def get_size_statistics(order, dishs, size, dish_stats):
    total_size = 0
    for dish, dish_stat in dish_stats.items():
        if dish_stat['total'] == 0:
            continue
        total_counts = dish_stat['total']
        total_size += total_counts * float(size[dish])
    return total_size 

@tracking 
def get_acc_statistics(acc, acc_eo, order, dishs, dish_stats):
    for dish, dish_stat in dish_stats.items():
        if dish_stat['total'] == 0:
            continue
        total_counts = dish_stat['total']
        total_exclude_order = dish_stat['total'] - dish_stat['order']
        acc_eo[dish] += total_exclude_order
        acc[dish] += total_counts
    return acc_eo, acc

@tracking
def if_stock_available(order, dishs, acc, limit, dish_stats):
    is_pendding = True
    for dish, dish_stat in dish_stats.items():
        if dish_stat['total'] == 0:
            continue

        total_counts = dish_stat['total']
        if acc[dish] + total_counts > int(limit[dish]):
            #order['Status'] = 'Pending'
            #status = 'Pending'
            is_pendding = False
            break
    return is_pendding

@tracking
def get_cash_on_delivery_fee(order, total_price_and_shipping_fee, contain_appredix):
    print(contain_appredix)
    print(order[lang.order_shipping_fee])
    print(order[lang.order_delivery].strip())
    if u'免運' in [order[lang.order_shipping_fee]] or contain_appredix:
        return 0
    print('pass1')
    if  order[lang.order_delivery].strip() in [u'自取', u'隨貨寄出']:
        return 0
    print('pass2')
    print(total_price_and_shipping_fee)
    print(type(total_price_and_shipping_fee))
    #2,000元以下 30元    
    #2,001 ~ 5,000元 60元
    #5,001 ~ 10,000元    90元

    if total_price_and_shipping_fee >= 20000:
        return 0
    elif total_price_and_shipping_fee >= 2000:
        return 60
    else:
        return 30

@tracking 
def get_paid_status(order, paid_dwks):
    """ return
    0: doesn't paid
    1: exact match 
    2: paid, but number doesn't match. check manually
    """
    try:
        # Refactor the follwoing 
        #price = order[lang.order_amount][order[lang.order_amount].index(u")")+1:].strip()
        price = order[lang.order_amount][:order[lang.order_amount].index(u"(")].strip()
        #price = re.sub('([^)]+)', '', order[u'Total']).replace(u")","").strip()
        order_price = int(price)
    except:
        return 0, ""

    for idx, row in paid_dwks.iteritems():
        if row[lang.order_sn] != order[lang.order_sn]:
            continue
        print(order_price, price, type(row[u'金額']), type(price))
        row[u'對應'] = order[lang.fb_id]
        if price == row[u'金額'] or order[lang.order_cod] == u'是':
            # exact match 
            return 1, row[u'匯款帳號']
        if price != row[u'金額']:
            # u"需手動檢查 %s"
            return 2, row[u'匯款帳號']
    return 0, ""

@tracking 
def get_individual_shipping_fee(package_size, ship_vendor):
    #if order[u'宅配方式'].strip() == u'宅配通':
    if u'宅配通' in ship_vendor.strip():
        if package_size == 'xs':
            return 140
        if package_size == 's':
            return 140
        if package_size == 'm':
            return 190
        if package_size == 'l':
            return 240

    if u'黑貓' in ship_vendor.strip():
        if package_size == 'xs':
            return 170
        if package_size == 's':
            return 170
        if package_size == 'm':
            return 235
        if package_size in ['l', 'xl']:
            return 250
    raise Exception('get_individual_shipping_fee', package_size, ship_vendor, traceback.format_exc())


@tracking 
def get_shipping_fee(order, package_size, total_price, contain_appredix):
    try:
        if u'免運' in order[lang.order_shipping_fee] or contain_appredix:
            return  0
        #if total_price >= 3000 or order[lang.order_delivery].strip() in [u'自取', u'隨貨寄出']:
        if  order[lang.order_delivery].strip() in [u'自取', u'隨貨寄出']:
            return 0

        packages = package_size.split("+")
        total = 0
        for package in packages:
            total += get_individual_shipping_fee(package, order[lang.order_delivery])
        if total >= 250:
            total = 250
        return total

    except Exception as e:
        msg = u"%s,  %s , %s can't get shipping fee %s"  % (order['idx'], order[u"訂單編號"],
                                                         order[lang.order_delivery], order[u'宅配方式'])
        print('exception 628', msg)
        print( order[u'宅配方式'] )
        raise Exception('get_shipping_fee', package_size, total_price, contain_appredix, traceback.format_exc())

@tracking
def get_package_size(total_size):
    if total_size > 14:
        return "xl+%s" % get_package_size(total_size-14) 
    if total_size <= 1.2:
        return 'xs'
    if total_size <= 4:
        return 's'
    if total_size <= 7:
        return 'm'
    if total_size <= 14:
       return 'l'
    raise Exception('get_package_size', total_size, traceback.format_exc())

"""
@tracking
def get_user_id(order, user_dwks):
    for idx, user in user_dwks.iteritems():
        for account in [lang.fb_id]:
            if order[account] != "" and order[account] == user[account]:
                return idx, account, order[account]
    # Not exist in user table
    user, account_type, account  = create_new_user_record(order, user_dwks)
    return user['idx'], account_type, account

@tracking
def check_if_paid(order_id, paid_dwks):
    for idx, pay in paid_dwks.items():
        if order_id == pay[lang.order_sn]:
            return "Y"
    return "N"
@tracking
def create_new_user_record(order, users_dwks): 
    user = {}
    account  = ""
    for account_type in [lang.fb_id]:
        if order[account_type] != "":
           user[account_type] = order[account_type] 
           accout = user[account_type]
           break
    user[u'手機'] = order[u'手機']
    '''
    user[u'姓名'] = order.get(u'姓名', "")
    user[u'地址'] = order.get(u'地址', "")
    user[u'MAIL'] = order.get(u'MAIL', "")
    '''
    user[''] = users_dwks.__len__ + 1
    user_row = users_dwks.append(user)
    return user_row, account_type, account 
@tracking
def get_order_id(pno, user_id):
    # P00#ID
    return pno + "-" + str(user_id)
"""

@tracking
def generate_hidden_sheet(row, orders, hidden_sh):
    hidden_wks = hidden_sh.worksheet(row['No'])
    hidden_dwks = DictSheet(wks=hidden_wks)
    for idx, order in orders.items():
        data_dict = {}
        if order['Status'] == "ok":
            for k, v in order.items():
                if k in hidden_dwks.mapping:
                    data_dict[k] = v
            hidden_dwks[idx] = data_dict
            """
            for k, v in hidden_dwks[idx].items():
                v = order[k]
            """
@tracking   
def get_dishs(pno, limit):
    #print pno, type(limit), "limit"
    dish_list = []
    for dish, idx in limit.mapping.items():
        if dish == "No":
            continue
        dish_list.append(dish)
    #pp(dish_list)
    return dish_list

@tracking
def if_order_contain_appredix(order, price, dishs, dish_stats):
    # 是否有包含補
    contain = False
    for dish, dish_stat in dish_stats.items():
        if dish_stat['total'] == 0:
            continue
        appendix = dish_stat['appendix']
        if appendix != 0:
            return True
    return contain

@tracking
def get_order_total(order, price, dishs, dish_stats):
    # input: price is dict, order is row
    total = 0
    empty_order = True
    for dish, dish_stat in dish_stats.items():
        if dish_stat['total'] == 0:
            continue
        empty_order = False
        total += dish_stat['order'] * price[dish]
    return empty_order, total

@tracking
def process_hidden_sheet(orders, hidden_dwks):
    pass

"""
def process_report():
    pass
"""

@tracking
def get_sh(uri, url=None):
    scope = ['https://spreadsheets.google.com/feeds']
    credentials = ServiceAccountCredentials.from_json_keyfile_name('facebook-shop-cart-acdb9ee1b37e.json', scope)
    gc = gspread.authorize(credentials)
    print('uri',uri)
    if url is None:
        sh = gc.open(uri)
    else:
        sh = gc.open_by_key(uri)
    return sh

process_control()
