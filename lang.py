# -*- coding: utf-8 -*-

# -------  order sheet -------
order_sn = u'訂單編號'
fb_id = u'FB代號'
order_status = u'Status'
payment_last_5_pin = u'匯款帳號'
order_amount = u'Total'
order_size = u'Size'
order_shipping_fee = u'運費'
order_delivery = u'宅配方式'
# cash on delivery (COD) 貨到付款
order_cod = u'到付'
order_msg = u'留言'
order_memo = u'Memo'

status_amount = u"統計"
status_total_counts = u"總金額"


# -------  setting sheet -------
items = u'整合表單'
close = u'關團'
last_check_time = u'最後檢查時間'
check_result = u'檢查成功'
#u'宅配單檢查'
#u'對帳單分頁檢查'
